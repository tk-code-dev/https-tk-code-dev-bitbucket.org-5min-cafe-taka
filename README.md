## 5min Cafe 

Users are able to get info of Cafe shops, the location is within 5 min walking distance from their current location, and display the time required.

---

Features
- GoogleMap Style 
  https://mapstyle.withgoogle.com/
---

Built with
- Foursquare api, Retrofit, Gson, RecyclerView
