package com.tk.code.guaranaandroid;

import com.google.gson.annotations.SerializedName;

import java.util.List;

class MapInfoResponse {
    @SerializedName("meta")
    public Meta meta;
    @SerializedName("response")
    public Response response;
}
class Meta {
    @SerializedName("code")
    public int code;
    @SerializedName("requestId")
    public String requestId;
}
class Response {
    @SerializedName("venues")
    public List<Venues> venues;
}

class Venues {
    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("location")
    public F_Location location;
    @SerializedName("hours")
    public Hours hours;
}

class F_Location {
    @SerializedName("lat")
    public float lat;
    @SerializedName("lng")
    public float lng;
    @SerializedName("address")
    public String address;
    @SerializedName("distance")
    public int distance;
}

class Hours {
    @SerializedName("isOpen")
    public boolean isOpen;
}


