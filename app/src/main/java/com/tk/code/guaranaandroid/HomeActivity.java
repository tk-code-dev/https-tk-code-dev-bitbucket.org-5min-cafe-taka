package com.tk.code.guaranaandroid;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.tk.code.guaranaandroid.model.Stores;
import com.tk.code.guaranaandroid.ui.HomeActivityAdapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class HomeActivity extends FragmentActivity implements OnMapReadyCallback {
//    https://api.foursquare.com/v2/venues/search?near=montreal,CA&query=coffee&v=20200213&m=foursquare&client_secret=ETEJMZUJTUPRFZFKNIPMHGQ1HVR15ULLT5KSQMX2HOEVWMJT&client_id=EVVKRMSVO0Y2VYIIMUAFJGPIAO3EGGXIY4QDJGH3SNWKAPHF
//    https://api.foursquare.com/v2/venues/search?ll=40.7484,-73.&query=coffee&client_secret=ETEJMZUJTUPRFZFKNIPMHGQ1HVR15ULLT5KSQMX2HOEVWMJT&client_id=EVVKRMSVO0Y2VYIIMUAFJGPIAO3EGGXIY4QDJGH3SNWKAPHF"

    private static final String TAG = HomeActivity.class.getSimpleName();

    public static String BaseUrl = "https://api.foursquare.com/";
    //
    private Stores stores;
    private List<Stores> storeList;
    private List<LatLng> latLngs = new ArrayList<>();
    private List<Integer> shopIcons = new ArrayList<>();
    private List<String> shopName = new ArrayList<>();
    private List<Integer> shopDistance = new ArrayList<>();
    private List<String> shopOpen = new ArrayList<>();
    private RecyclerView recyclerView;

    Location currentLocation;
    private static LatLng initialPosition;
    FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;
    private static String ll = "";

    private static GoogleMap mMap;
    private static Marker marker;

    HomeActivityAdapter adapter;
    RecyclerView.Adapter rAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Button button = findViewById(R.id.button);
        Button buttonMap = findViewById(R.id.button_map);
        storeList = new ArrayList<>();
        storeList.clear();

        recyclerView = findViewById(R.id.recyclerView);

        // use this setting to improve performance
        recyclerView.setHasFixedSize(true);


        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

//                getCurrentDetail(storeList.get(0).id);

            }
        });

        buttonMap.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                onResume();
//                getCurrentDetail(storeList.get(0).id);

            }
        });

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        markerOn();

                        // FIX ME

//                    Toast.makeText(getApplicationContext(), ll, Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getApplicationContext(), currentLocation.getLatitude() + "" + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();

//                        CameraUpdate cu = CameraUpdateFactory.newLatLng(new LatLng(storeList.get(position).lat, storeList.get(position).lng));
//                        mMap.animateCamera(cu);

                        double d = Double.valueOf(storeList.get(position).getDistance());
                        double min = (d + 30) / 80;  // add 30m
                        convertTime(min);

                        double latitude = storeList.get(position).lat;
                        double longitude = storeList.get(position).lng;
                        LatLng mLatLng = new LatLng(latitude, longitude);
                        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(storeList.get(position).storeName).icon(BitmapDescriptorFactory.fromResource(R.drawable.cafe_cup_click)));

                        CameraUpdate update = CameraUpdateFactory.newLatLng(mLatLng);
                        mMap.animateCamera(update);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        // use a linear layout manager
        RecyclerView.LayoutManager rLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(rLayoutManager);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLocation();


    }

    private void fetchLocation() {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    ll = currentLocation.getLatitude() + "," + currentLocation.getLongitude();
//                    Toast.makeText(getApplicationContext(), ll, Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getApplicationContext(), currentLocation.getLatitude() + "" + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                    Log.i("ll", ll);
                    initialPosition = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                    getCurrentData(ll);


                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                    assert supportMapFragment != null;
                    supportMapFragment.getMapAsync(HomeActivity.this);
                }
            }
        });

    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            mMap = googleMap;
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.map_style2));
            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        final LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title("I am here!")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin));
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
        googleMap.addMarker(markerOptions);

        Circle circle = googleMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(400)
                .strokeColor(Color.WHITE));

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng tapLocation) {
//                Log.i("ll_b",storeList.get(0).getLat()+"");

                for (int i = 0; i < storeList.size(); i++) {
                    LatLng newLocation = new LatLng(storeList.get(i).getLat(), storeList.get(i).getLng());
                    latLngs.add(newLocation);
                    googleMap.addMarker(new MarkerOptions().position(latLngs.get(i)).title(storeList.get(i).storeName).icon(BitmapDescriptorFactory.fromResource(R.drawable.cafe_cup)));
                }
            }
        });


        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {

                final LatLng markerPosition = marker.getPosition();
                int selected_marker = -1;
                for (int i = 0; i < storeList.size(); i++) {
                    if (markerPosition.latitude == storeList.get(i).getLat() && markerPosition.longitude == storeList.get(i).getLng()) {
                        selected_marker = i;
                    }
                }
                CameraPosition cameraPosition = new CameraPosition.Builder().target(markerPosition).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                recyclerView.smoothScrollToPosition(selected_marker);


                marker.showInfoWindow();

                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        HomeActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                marker.hideInfoWindow();
                            }
                        });
                    }
                }, 2500);
                return false;

            }
        });


//        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//                Toast.makeText(getApplicationContext(), marker.getTitle(), Toast.LENGTH_LONG).show();
//                return false;
//            }
//        });

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
//                Toast.makeText(getApplicationContext(), marker.getTitle(), Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLocation();
                }
                break;
        }
    }


    void markerOn() {
        for (int i = 0; i < storeList.size(); i++) {
            LatLng newLocation = new LatLng(storeList.get(i).getLat(), storeList.get(i).getLng());
            latLngs.add(newLocation);
            mMap.addMarker(new MarkerOptions().position(latLngs.get(i)).title(storeList.get(i).storeName).icon(BitmapDescriptorFactory.fromResource(R.drawable.cafe_cup)));
        }
    }

    void getCurrentData(String ll) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MapInfoService service = retrofit.create(MapInfoService.class);

        Call<MapInfoResponse> call = service.getInfo(ll);

        call.enqueue(new Callback<MapInfoResponse>() {

            @Override
            public void onResponse(@NonNull Call<MapInfoResponse> call, @NonNull Response<MapInfoResponse> response) {
                Log.i("response_code", response.code() + "");

                if (response.code() == 200) {
                    MapInfoResponse mapInfoResponse = response.body();
                    assert mapInfoResponse != null;
                    int venuesNum = mapInfoResponse.response.venues.size();

                    for (int i = 0; i < venuesNum; i++) {

                        String id = mapInfoResponse.response.venues.get(i).id;
                        String name = mapInfoResponse.response.venues.get(i).name;
                        double lat = mapInfoResponse.response.venues.get(i).location.lat;
                        double lng = mapInfoResponse.response.venues.get(i).location.lng;
                        int distance = mapInfoResponse.response.venues.get(i).location.distance;
                        String address = mapInfoResponse.response.venues.get(i).location.address;
                        if (address == null) {
                            address = " - - - ";
                        }
                        Log.i("address", address);

                        stores = new Stores(id, name, address, distance, "", "", 1, lat, lng, false, false);
                        storeList.add(stores);
//                        shopIcons.add(R.drawable.cafe_cup);
//                        shopName.add(name);
//                        shopOpen.add("open");
//                        shopDistance.add(distance);
                    }

                    Collections.sort(storeList, new CompareDistance());
                    for (Stores c : storeList) {
                        System.out.println(c.storeName + ", " + c.getDistance());
                        shopIcons.add(R.drawable.cafe_cup);
                        shopName.add(c.storeName);
                        shopOpen.add(c.getAddress()); // open
                        shopDistance.add(c.distance);
                    }
                    //  Sorting by Num of Employees

                    for (int i = 0; i < storeList.size(); i++) {
                        Log.i("list", storeList.get(i).getId() + " : " + storeList.get(i).getStoreName() + " " + storeList.get(i).getDistance()
                                + "m lat" + storeList.get(i).getLat() + " lng" + storeList.get(i).getLng());
                    }

                   rAdapter = new HomeActivityAdapter(shopIcons, shopName, shopDistance, shopOpen);
                    recyclerView.setAdapter(rAdapter);
                }
            }

            @Override
            public void onFailure(Call<MapInfoResponse> call, Throwable t) {
                Log.i("store_info", "error");
            }
        });
    }

    public class CompareDistance implements Comparator<Stores> {
        public int compare(Stores c1, Stores c2) {
            if (c1.getDistance() < c2.getDistance()) {
                return -1;
            } else if (c1.getDistance() > c2.getDistance()) {
                return 1;
            } else {
                return 0;
            }
        }
    }


    void getCurrentDetail(String id) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MapInfoService service = retrofit.create(MapInfoService.class);
//        Log.d("store_name", query);

        Call<MapInfoResponse> call = service.getInfoMore(id);

        call.enqueue(new Callback<MapInfoResponse>() {

            @Override
            public void onResponse(@NonNull Call<MapInfoResponse> call, @NonNull Response<MapInfoResponse> response) {
                Log.i("response_code", response.code() + "");

                if (response.code() == 200) {
                    MapInfoResponse mapInfoResponse = response.body();
                    assert mapInfoResponse != null;
//                    int venuesNum = mapInfoResponse.response.venues.size();


//                    String id = mapInfoResponse.response.venues.get(0).id;
//                    String name = mapInfoResponse.response.venues.get(0).name;
//                    double lat = mapInfoResponse.response.venues.get(0).location.lat;
//                    double lng = mapInfoResponse.response.venues.get(0).location.lng;
//                    int distance = mapInfoResponse.response.venues.get(0).location.distance;
//                    String formattedAddress = mapInfoResponse.response.venues.get(0).location.address;
                    boolean isOpen = mapInfoResponse.response.venues.get(0).hours.isOpen;

                    Log.i("isOpen", isOpen + "");
//                    stores = new Stores(id, name, formattedAddress, distance, "", "", 1, lat, lng, isOpen, false);
//                    storeList.add(stores);
//                        shopIcons.add(R.drawable.cafe_cup);
//                        shopName.add(name);
//                        shopOpen.add("open");
//                        shopDistance.add(distance);
                }
            }

            @Override
            public void onFailure(Call<MapInfoResponse> call, Throwable t) {
                Log.i("store_info", "error");
            }
        });
    }

    public void convertTime(Double min) {
        Toast toast;
        BigDecimal bigDecimal = new BigDecimal(String.valueOf(min));
        int intValue = bigDecimal.intValue();
        String decimal = bigDecimal.subtract(new BigDecimal(intValue)).toPlainString();
        String decimalValue = String.format("%.0f", Double.parseDouble(decimal) * 60);
//        return decimalValue;
        if (intValue == 0) {
            String time = decimalValue + "sec";
            toastMake(time, getApplication());
//            toast = Toast.makeText(getBaseContext(), decimalValue + "sec", Toast.LENGTH_SHORT);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
        } else {
            String time = intValue + "min" + decimalValue + "sec";
            toastMake(time, getApplication());
//            toast = Toast.makeText(getBaseContext(), intValue + "min" + decimalValue + "sec", Toast.LENGTH_SHORT);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
        }
    }

    // custom toast maker
    public void toastMake(String str, Context cont) {
        Toast toast = new Toast(cont);

        LayoutInflater inflater = getLayoutInflater();

        // relative_layout:custom_toast.xmlのRelativeLayoutに付けたID
        ViewGroup viewGroup = findViewById(R.id.toast_layout);

        // inflateする
        View view = inflater.inflate(R.layout.custom_toast, viewGroup);

        TextView textView = view.findViewById(R.id.message);
        textView.setText(str);

        toast.setView(view);
        // 表示時間
        toast.setDuration(Toast.LENGTH_SHORT);
        // 位置調整
        toast.setGravity(Gravity.CENTER, 0, -100);

        toast.show();
    }
}