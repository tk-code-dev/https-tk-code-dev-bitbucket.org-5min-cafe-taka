package com.tk.code.guaranaandroid.model;

import java.io.Serializable;

public class Stores implements Serializable {
    public String id;
    public String storeName;
    public String address;
    public int distance;
    public String description;
    public String photoUrl;
    public float rating;
    public double lat;
    public double lng;
    public boolean isOpen;
    public boolean isFavorite;

    public Stores(String id) {
        this.id = id;
    }

    public Stores(String id, String storeName, String address, int distance, String description, String photoUrl, float rating, double lat, double lng, boolean isOpen, boolean isFavorite) {
        this.id = id;
        this.storeName = storeName;
        this.address = address;
        this.distance = distance;
        this.description = description;
        this.photoUrl = photoUrl;
        this.rating = rating;
        this.lat = lat;
        this.lng = lng;
        this.isOpen = isOpen;
        this.isFavorite = isFavorite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}
