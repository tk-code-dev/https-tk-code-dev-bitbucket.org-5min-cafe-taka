package com.tk.code.guaranaandroid;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MapInfoService2 {
    @GET("v2/venues/{id}?v=20200213&client_secret=ETEJMZUJTUPRFZFKNIPMHGQ1HVR15ULLT5KSQMX2HOEVWMJT&client_id=EVVKRMSVO0Y2VYIIMUAFJGPIAO3EGGXIY4QDJGH3SNWKAPHF")
    Call<MapInfoResponse> getInfoMore(@Path("id") String id);
}
