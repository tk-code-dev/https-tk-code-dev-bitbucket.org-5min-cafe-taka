package com.tk.code.guaranaandroid.ui;

//AndroidX

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tk.code.guaranaandroid.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeActivityAdapter extends RecyclerView.Adapter<HomeActivityAdapter.ViewHolder> {

    private List<Integer> iImages;
    private List<String> iNames;
    private List<Integer> iDistance;
    private List<String> isOpen;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.store_info, parent, false);

        // view's size, margins, paddings and layout parameters
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.imageView.setImageResource(iImages.get(position));
        holder.nameView.setText(iNames.get(position));
        holder.distanceView.setText(iDistance.get(position)+"m");
        holder.openView.setText(isOpen.get(position));
    }

    @Override
    public int getItemCount() {
        return iNames.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        // data item
        ImageView imageView;
        TextView nameView;
        TextView distanceView;
        TextView openView;
        LinearLayout shopLayout;


        ViewHolder(View v) {
            super(v);
            imageView = v.findViewById(R.id.cupIV);
            nameView = v.findViewById(R.id.shopNameTV);
            distanceView = v.findViewById(R.id.distanceTV);
            openView = v.findViewById(R.id.address_TV);
            shopLayout = v.findViewById(R.id.cafe_layout);

        }
    }

    // Provide a suitable constructor
    public HomeActivityAdapter(List<Integer> itemImages, List<String> itemNames, List<Integer> distance, List<String> isOpen) {
        this.iImages = itemImages;
        this.iNames = itemNames;
        this.iDistance = distance;
        this.isOpen = isOpen;
    }


}