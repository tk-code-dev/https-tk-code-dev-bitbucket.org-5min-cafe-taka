package com.tk.code.guaranaandroid;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MapInfoService {
    @GET("v2/venues/search?query=cafe&v=20200219&radius=300&m=foursquare&client_secret=ETEJMZUJTUPRFZFKNIPMHGQ1HVR15ULLT5KSQMX2HOEVWMJT&client_id=EVVKRMSVO0Y2VYIIMUAFJGPIAO3EGGXIY4QDJGH3SNWKAPHF")
    Call<MapInfoResponse> getInfo(@Query("ll") String ll);


    @GET("v2/venues/{id}?v=20200220&client_secret=ETEJMZUJTUPRFZFKNIPMHGQ1HVR15ULLT5KSQMX2HOEVWMJT&client_id=EVVKRMSVO0Y2VYIIMUAFJGPIAO3EGGXIY4QDJGH3SNWKAPHF")
    Call<MapInfoResponse> getInfoMore(@Path("id") String id);
}
//@GET("v2/venues/search?near=montreal,CA&query=coffee&v=20200213&m=foursquare&client_secret=SLZOPYABHLNEXAQMYNPQNASDHSKGOOBQHAUIN3MYE2WZSHXJ&client_id=UOJ24NDYP4VJG2SGVMEKRNE0D4EFT30T0EF0IEOSI4DB3DJ2")
//venues/search?ll=40.7484,-73.9857&query=coffee
//    @GET("v2/venues/search?ll=40.7484,-73.&query=coffee&client_secret=ETEJMZUJTUPRFZFKNIPMHGQ1HVR15ULLT5KSQMX2HOEVWMJT&client_id=EVVKRMSVO0Y2VYIIMUAFJGPIAO3EGGXIY4QDJGH3SNWKAPHF")
